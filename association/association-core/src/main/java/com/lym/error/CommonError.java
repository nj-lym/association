package com.lym.error;

/**
 * Created by rf on 2019/3/10.
 */
public interface CommonError  {
    int getCode();
    String getMsg();
}
