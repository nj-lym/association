package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.PassageType;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */

public interface PassageTypeMapper extends BaseMapper<PassageType> {

}
