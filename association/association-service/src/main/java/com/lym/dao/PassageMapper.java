package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.dto.PassageTypeDto;
import com.lym.dto.PassageFileDto;
import com.lym.entity.Passage;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 * @author lym
 * @since 2021-03-03
 */
public interface PassageMapper extends BaseMapper<Passage> {
    List<PassageTypeDto> findPassage(Passage passage);

    PassageFileDto getInfoById(Integer id);

    Passage findSimplePassage(Passage passage);

    int add(Passage passage);
}
