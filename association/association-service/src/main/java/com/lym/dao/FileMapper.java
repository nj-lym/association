package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.File;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-04-08
 */
public interface FileMapper extends BaseMapper<File> {
    Integer save(File file);
}
