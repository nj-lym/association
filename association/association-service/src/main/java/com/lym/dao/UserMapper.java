package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 * @author lym
 * @since 2021-03-03
 */

public interface UserMapper extends BaseMapper<User> {

    List<User> getUsersPage(String name, String roleId);

    User getUsersById(Integer id);

    List<User> getUsersList(String account);

    Integer updateUserById(@Param("user") User user);
}
