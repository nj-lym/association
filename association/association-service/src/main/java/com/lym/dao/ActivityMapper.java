package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.dto.ActivityFileDto;
import com.lym.dto.ActivityTypeDto;
import com.lym.entity.Activity;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-04-08
 */
public interface ActivityMapper extends BaseMapper<Activity> {
    List<ActivityTypeDto> findActivity(Activity activity);

    ActivityFileDto getInfoById(Integer id);

    Integer add(Activity activity);
}
