package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.ActivityType;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-04-17
 */
public interface ActivityTypeMapper extends BaseMapper<ActivityType> {

}
