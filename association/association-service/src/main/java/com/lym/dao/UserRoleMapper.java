package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.UserRole;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
