package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.UserRank;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
public interface UserRankMapper extends BaseMapper<UserRank> {

}
