package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.Club;
import com.lym.vo.ClubInfoVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-04-14
 */
public interface ClubMapper extends BaseMapper<Club> {
    List<ClubInfoVo> findClub(Club club);

    ClubInfoVo getInfoById(Integer id);
}
