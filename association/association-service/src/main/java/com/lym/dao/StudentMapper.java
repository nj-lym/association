package com.lym.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lym.entity.Student;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lym
 * @since 2021-04-14
 */
public interface StudentMapper extends BaseMapper<Student> {

}
