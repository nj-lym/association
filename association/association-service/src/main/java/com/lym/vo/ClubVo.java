package com.lym.vo;

import com.lym.entity.Club;
import com.lym.entity.ClubType;
import lombok.Data;

import java.util.List;

/**
 * Created by rf on 2019/4/18.
 */
@Data
public class ClubVo {
    private ClubType clubType;
    private List<Club> clubList;

    public ClubVo(ClubType clubType, List<Club> clubList) {
        this.clubType = clubType;
        this.clubList = clubList;
    }
}
