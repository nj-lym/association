package com.lym.vo;

import com.lym.entity.Club;
import com.lym.entity.ClubType;
import com.lym.entity.File;
import lombok.Data;

import java.util.List;

/**
 * Created by rf on 2019/5/3.
 */
@Data
public class ClubInfoVo extends Club {
    private ClubType clubType;
    private List<File> fileList;
}
