package com.lym.dto;

import com.lym.entity.File;
import com.lym.entity.Passage;
import lombok.Data;

import java.util.List;

/**
 * Created by rf on 2019/4/28.
 */
@Data
public class PassageFileDto extends Passage{
    private List<File> fileList;
}
