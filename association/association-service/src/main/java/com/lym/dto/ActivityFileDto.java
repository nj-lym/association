package com.lym.dto;

import com.lym.entity.Activity;
import com.lym.entity.File;
import lombok.Data;

import java.util.List;

/**
 * Created by rf on 2019/4/28.
 */
@Data
public class ActivityFileDto extends Activity{
    private List<File> fileList;
}
