package com.lym.dto;

import com.lym.entity.Activity;
import com.lym.entity.ActivityType;
import lombok.Data;

/**
 * Created by rf on 2019/5/8.
 */
@Data
public class ActivityTypeDto extends Activity{
    private ActivityType activityType;
}
