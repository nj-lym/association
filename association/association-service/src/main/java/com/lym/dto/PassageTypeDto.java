package com.lym.dto;

import com.lym.entity.Passage;
import com.lym.entity.PassageType;
import lombok.Data;

/**
 * Created by rf on 2019/5/8.
 */
@Data
public class PassageTypeDto extends Passage{
    private PassageType passageType;
}
