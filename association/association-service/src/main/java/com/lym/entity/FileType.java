package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 * @author lym
 * @since 2021-04-27
 */
@Data
@TableName("t_sys_file_type")
public class FileType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 社团类型
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 社团类型
     */
    private String type;


    @Override
    public String toString() {
        return "FileType{" + "id=" + id + ", type=" + type + "}";
    }
}
