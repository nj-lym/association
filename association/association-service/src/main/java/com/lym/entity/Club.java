package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 * @author lym
 * @since 2021-04-17
 */
@Data
@TableName("t_sys_club")
public class Club implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 社团编号
     */
    @TableId(value = "num")
    private Integer num;

    /**
     * 社团名称
     */
    private String name;

    /**
     * 社团简介
     */
    private String introduce;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 社团类型id
     */
    private Integer clubTypeId;

    /**
     * 社团状态
     */
    private Integer status;


    @Override
    public String toString() {
        return "Club{" + "num=" + num + ", name=" + name + ", introduce=" + introduce + ", createTime=" + createTime + ", clubTypeId=" + clubTypeId + ", status=" + status + "}";
    }
}
