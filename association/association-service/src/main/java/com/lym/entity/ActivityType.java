package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lym
 * @since 2021-04-17
 */
@Data
@TableName("t_sys_activity_type")
public class ActivityType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动类型
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 活动类型
     */
    @NotBlank(message = "活动类型名称不能为空")
    private String type;

    @Override
    public String toString() {
        return "ActivityType{" +
        "id=" + id +
        ", type=" + type +
        "}";
    }
}
