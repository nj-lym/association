package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 * @author lym
 * @since 2021-03-03
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@TableName("t_sys_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    @TableField(value = "id")
    private Integer userId;

    private String name;

    /**
     * 性别（0保密，1男，2女）
     */
    private Integer sex;

    /**
     * 学院
     */
    private String institute;

    @NotNull(message = "用户账号不能为空")
    //    @Email(message = "请输入合法邮箱")
    private String account;

    //    @JsonIgnore
    @NotNull(message = "用户密码不能为空")
    //    @Length(min=6,max=16,message = "密码至少6位，最多16位")
    private String password;

    /**
     * 用户创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy年MM月dd")
    @DateTimeFormat(pattern = "yyyy年MM月dd")
    private Date createTime;

    /**
     * 用户是否激活（默认0，未激活；1激活）
     */
    @TableField(value = "is_active")
    private Integer isActive;

    private String roleId;

    @TableField(exist = false)
    private String roleName;

    /**
     * 用户职称
     */
    @TableField(value = "call_name")
    private String callName;

    private String lastLoginTime;

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", name=" + name + ", sex=" + sex + ", institute=" + institute + ", account=" + account + ", password=" + password + ", createTime=" + createTime + ", isActive=" + isActive + ", lastLoginTime=" + lastLoginTime + "}";
    }
}
