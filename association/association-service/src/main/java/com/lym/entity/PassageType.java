package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 * @author lym
 * @since 2021-03-03
 */
@Data
@TableName("t_sys_passage_type")
public class PassageType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文章类型
     */
    private String id;

    /**
     * 文章类型
     */
    private String type;


    @Override
    public String toString() {
        return "PassageType{" + "id=" + id + ", type=" + type + "}";
    }
}
