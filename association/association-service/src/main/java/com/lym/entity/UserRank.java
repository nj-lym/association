package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@Data
public class UserRank implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 职称id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 职称名称
     */
    private String rankName;


    @Override
    public String toString() {
        return "UserRank{" +
        "id=" + id +
        ", rankName=" + rankName +
        "}";
    }
}
