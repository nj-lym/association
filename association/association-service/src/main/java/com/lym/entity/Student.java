package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 * @author lym
 * @since 2021-04-14
 */
@Data
@TableName("t_sys_student")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String num;

    private String name;

    /**
     * 性别
     */
    private Integer sex;

    private Integer age;

    /**
     * 电子邮箱
     */
    private String email;

    private String phone;

    /**
     * 社团编号
     */
    private Integer clubNum;


    @Override
    public String toString() {
        return "Student{" + "num=" + num + ", name=" + name + ", sex=" + sex + ", age=" + age + ", email=" + email + ", phone=" + phone + ", clubNum=" + clubNum + "}";
    }
}
