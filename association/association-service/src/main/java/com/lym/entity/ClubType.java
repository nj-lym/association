package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 * @author lym
 * @since 2021-04-14
 */
@Data
@TableName("t_sys_club_type")
public class ClubType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文章类型
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文章类型
     */
    @NotBlank(message = "社团类型不能为空")
    private String type;


    @Override
    public String toString() {
        return "ClubType{" + "id=" + id + ", type=" + type + "}";
    }
}
