package com.lym.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 * @author lym
 * @since 2021-04-08
 */
@Data
@TableName("t_sys_one_column")
public class OneColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 社团名称
     */
    private String clubName;

    /**
     * 社团概况
     */
    private String clubSummary;

    /**
     * 社团简介
     */
    private String introduce;

    /**
     * 社团快讯
     */
    private String notice;

    /**
     * 社团活动风采
     */
    private String show;

    @Override
    public String toString() {
        return "OneColumn{" + "id=" + id + ", clubName=" + clubName + ", clubSummary=" + clubSummary + ", introduce=" + introduce + ", notice=" + notice + ", show=" + show + "}";
    }
}
