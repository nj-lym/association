package com.lym.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lym.dao.PassageTypeMapper;
import com.lym.entity.PassageType;
import com.lym.service.PassageTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@Service
public class PassageTypeServiceImpl extends ServiceImpl<PassageTypeMapper, PassageType> implements PassageTypeService {

}
