package com.lym.service;

import com.lym.dto.PassageTypeDto;
import com.lym.dto.PassageFileDto;
import com.lym.entity.Passage;
import com.lym.vo.PassagePageVo;
import com.lym.vo.PassageVo;
import com.github.pagehelper.PageInfo;

import java.io.Serializable;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
public interface PassageService {
    PassageVo getTopN(int n, Passage search);


    PageInfo<PassageTypeDto> findPage(int pageNum, int pageSize, Passage search);

    PassagePageVo getPageWithTypeList(int pageNum, int pageSize, Integer passageTypeId);

    PassageFileDto getInfoById(Integer id);


    Passage add(Passage passage);

    PassageFileDto updateInfoById(PassageFileDto passage);

    boolean deleteById(Serializable id);
}
