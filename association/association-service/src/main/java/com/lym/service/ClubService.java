package com.lym.service;

import com.baomidou.mybatisplus.service.IService;
import com.lym.entity.Club;
import com.lym.vo.ClubInfoVo;
import com.lym.vo.ClubPageVo;
import com.lym.vo.ClubVo;
import com.github.pagehelper.PageInfo;

import java.io.Serializable;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lym
 * @since 2021-04-14
 */
public interface ClubService extends IService<Club> {
    ClubVo getTopN(int n, Club search);

    PageInfo<ClubInfoVo> getPage(int pageNum, int pageSize, Club search);

    ClubPageVo getPageWithTypeList(int pageNum, int pageSize, Integer clubTypeId);

    Club getById(Serializable id);

    Club add(Club club);

    Club updateWithId(Club club);

    boolean deleteById(Serializable id);
}
