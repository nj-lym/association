package com.lym.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lym.dao.OneColumnMapper;
import com.lym.entity.OneColumn;
import com.lym.service.OneColumnService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lym
 * @since 2021-04-08
 */
@Service
public class OneColumnServiceImpl extends ServiceImpl<OneColumnMapper, OneColumn> implements OneColumnService {

}
