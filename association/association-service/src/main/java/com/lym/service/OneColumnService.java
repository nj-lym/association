package com.lym.service;

import com.baomidou.mybatisplus.service.IService;
import com.lym.entity.OneColumn;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lym
 * @since 2021-04-08
 */
public interface OneColumnService extends IService<OneColumn> {

}
