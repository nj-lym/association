package com.lym.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lym.dao.UserRankMapper;
import com.lym.entity.UserRank;
import com.lym.service.UserRankService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@Service
public class UserRankServiceImpl extends ServiceImpl<UserRankMapper, UserRank> implements UserRankService {

}
