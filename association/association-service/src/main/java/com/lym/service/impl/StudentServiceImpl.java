package com.lym.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lym.dao.StudentMapper;
import com.lym.entity.Student;
import com.lym.service.StudentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lym
 * @since 2021-04-14
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

}
