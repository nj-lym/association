package com.lym.service;

import com.baomidou.mybatisplus.service.IService;
import com.lym.dto.ActivityFileDto;
import com.lym.dto.ActivityTypeDto;
import com.lym.entity.Activity;
import com.lym.vo.ActivityPageVo;
import com.lym.vo.ActivityVo;
import com.github.pagehelper.PageInfo;

import java.io.Serializable;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lym
 * @since 2021-04-08
 */
public interface ActivityService extends IService<Activity> {
    ActivityVo getTopN(int n, Activity search);

    PageInfo<ActivityTypeDto> getPage(int pageNum, int pageSize, Activity search);

    ActivityPageVo getPageWithTypeList(int pageNum, int pageSize, Integer activityTypeId);

    ActivityFileDto getInfoById(Integer id);

    Activity add(Activity activity);

    ActivityFileDto updateWithId(ActivityFileDto activity);

    boolean deleteById(Serializable id);
}
