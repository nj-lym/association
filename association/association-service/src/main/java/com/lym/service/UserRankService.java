package com.lym.service;

import com.baomidou.mybatisplus.service.IService;
import com.lym.entity.UserRank;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
public interface UserRankService extends IService<UserRank> {

}
