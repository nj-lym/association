package com.lym.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lym.dao.ActivityMapper;
import com.lym.dao.ActivityTypeMapper;
import com.lym.entity.ActivityType;
import com.lym.service.ActivityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@Service
public class ActivityTypeServiceImpl extends ServiceImpl<ActivityTypeMapper, ActivityType> implements ActivityTypeService {
    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private ActivityTypeMapper activityTypeMapper;

}
