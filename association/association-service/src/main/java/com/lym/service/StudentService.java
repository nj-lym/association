package com.lym.service;

import com.baomidou.mybatisplus.service.IService;
import com.lym.entity.Student;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lym
 * @since 2021-04-14
 */
public interface StudentService extends IService<Student> {

}
