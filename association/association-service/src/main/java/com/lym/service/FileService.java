package com.lym.service;

import com.baomidou.mybatisplus.service.IService;
import com.lym.entity.File;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lym
 * @since 2021-04-08
 */
public interface FileService extends IService<File> {
    Integer save(File file);

    PageInfo<File> getPage(int pageNum, int pageSize, File search);
}
