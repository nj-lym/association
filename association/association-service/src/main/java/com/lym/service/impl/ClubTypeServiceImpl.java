package com.lym.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lym.dao.ClubTypeMapper;
import com.lym.entity.ClubType;
import com.lym.service.ClubTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@Service
public class ClubTypeServiceImpl extends ServiceImpl<ClubTypeMapper, ClubType> implements ClubTypeService {

}
