package com.lym;

import com.lym.util.ApplicationContextInfoUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@MapperScan("com.lym.dao")
@EnableCaching
public class ClubApplication {

    public static void main(String[] args) {
       ApplicationContext applicationContext = SpringApplication.run(ClubApplication.class, args);
        ApplicationContextInfoUtils.printSystemInfo(applicationContext);
    }

}
