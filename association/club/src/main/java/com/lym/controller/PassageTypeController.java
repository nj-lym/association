package com.lym.controller;


import com.lym.entity.PassageType;
import com.lym.entity.ResponseResult;
import com.lym.service.PassageTypeService;
import com.lym.util.ResponseResultUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@RestController
@RequestMapping("/passageTypes")
@Api(tags = "社团管理系统前台文章类型接口")
public class PassageTypeController {
    @Autowired
    private PassageTypeService passageTypeService;
    @GetMapping
    public ResponseResult list() {
        List<PassageType> passageTypeList= passageTypeService.selectList(null);
        return ResponseResultUtil.renderSuccess(passageTypeList);
    }
}

