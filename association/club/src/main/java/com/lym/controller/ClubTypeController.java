package com.lym.controller;


import com.lym.entity.ClubType;
import com.lym.entity.ResponseResult;
import com.lym.service.ClubTypeService;
import com.lym.util.ResponseResultUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@RestController
@RequestMapping("/clubTypes")
@Api(tags = "社团管理系统前台社团类型接口")
public class ClubTypeController {
    @Autowired
    private ClubTypeService clubTypeService;
    @GetMapping
    public ResponseResult list() {
        List<ClubType> clubTypeList= clubTypeService.selectList(null);
        return ResponseResultUtil.renderSuccess(clubTypeList);
    }
}

