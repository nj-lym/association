package com.lym.controller;


import com.lym.entity.ActivityType;
import com.lym.entity.ResponseResult;
import com.lym.service.ActivityTypeService;
import com.lym.util.ResponseResultUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@RestController
@RequestMapping("/activityTypes")
@Api(tags = "前台活动类型接口")
public class ActivityTypeController {
    @Autowired
    private ActivityTypeService activityTypeService;
    @GetMapping
    public ResponseResult list() {
        List<ActivityType> activityTypeList= activityTypeService.selectList(null);
        return ResponseResultUtil.renderSuccess(activityTypeList);
    }
}

