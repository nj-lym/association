package com.lym;

import com.lym.util.ApplicationContextInfoUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@MapperScan("com.lym.dao")
@EnableCaching
@RestController
public class ClubAdminApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext =SpringApplication.run(ClubAdminApplication.class, args);
		//打印系统访问信息
		ApplicationContextInfoUtils.printSystemInfo(applicationContext);
	}

}
