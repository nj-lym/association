package com.lym.controller;


import com.lym.entity.UserRole;
import com.lym.entity.ResponseResult;
import com.lym.enums.ErrorEnum;
import com.lym.exception.ParamInvalidException;
import com.lym.service.UserRoleService;
import com.lym.util.ResponseResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 * @author lym
 * @since 2021-03-03
 */
@RestController
@RequestMapping("/userRoles")
@Slf4j
@Api(tags = "用户角色接口")
public class UserRoleController {
    @Autowired
    private UserRoleService userRoleService;

    @GetMapping
    public ResponseResult list() {
        List<UserRole> userRoleList = userRoleService.selectList(null);
        return ResponseResultUtil.renderSuccess(userRoleList);
    }

    @ApiOperation("通过id获取一个用户角色类型")
    @GetMapping("/getById")
    public ResponseResult getById(Integer id) {
        UserRole userRole = userRoleService.selectById(id);
        return ResponseResultUtil.renderSuccess(userRole);
    }

    @PutMapping("/update")
    @ApiOperation("更新用户角色类型")
    public ResponseResult edit(@RequestBody UserRole userRole, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            String msg = bindingResult.getFieldError().getDefaultMessage();
            log.error("{}", msg);
            throw new ParamInvalidException(ErrorEnum.INVALIDATE_PARAM_EXCEPTION.setMsg(msg));
        }
        userRoleService.updateById(userRole);
        return ResponseResultUtil.renderSuccess("更新用户角色类型成功");
    }

    @DeleteMapping("/delete")
    @ApiOperation("根据id删除用户角色类型")
    public ResponseResult delete(Integer id) {
        userRoleService.deleteById(id);
        return ResponseResultUtil.renderSuccess("删除用户角色类型成功");
    }

    @PostMapping
    @ApiOperation("添加用户角色类型")
    public ResponseResult add(@RequestBody UserRole userRole, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            String msg = bindingResult.getFieldError().getDefaultMessage();
            log.error("{}", msg);
            throw new ParamInvalidException(ErrorEnum.INVALIDATE_PARAM_EXCEPTION.setMsg(msg));
        }
        userRoleService.insert(userRole);
        return ResponseResultUtil.renderSuccess("添加用户角色类型成功");
    }
}

