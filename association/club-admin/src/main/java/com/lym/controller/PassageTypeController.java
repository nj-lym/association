package com.lym.controller;


import com.lym.entity.PassageType;
import com.lym.entity.ResponseResult;
import com.lym.enums.ErrorEnum;
import com.lym.exception.ParamInvalidException;
import com.lym.service.PassageTypeService;
import com.lym.util.ResponseResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@RequestMapping("/passageTypes")
@RestController
@Api(value = "社团管理系统后台文章类型接口",tags = "社团管理系统后台文章类型接口")
public class PassageTypeController {
    @Autowired
    private PassageTypeService passageTypeService;

    @GetMapping
    public ResponseResult list() {
        List<PassageType> passageTypeList = passageTypeService.selectList(null);
        return ResponseResultUtil.renderSuccess(passageTypeList);
    }

    @ApiOperation("通过id获取一个文章类型")
    @GetMapping("/getById")
    public ResponseResult getById(Integer id) {
        PassageType passageType = passageTypeService.selectById(id);
        return ResponseResultUtil.renderSuccess(passageType);
    }

    @PutMapping
    @ApiOperation("更新文章类型")
    public ResponseResult edit(@Valid @RequestBody PassageType passageType, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            String msg = bindingResult.getFieldError().getDefaultMessage();
            throw new ParamInvalidException(ErrorEnum.INVALIDATE_PARAM_EXCEPTION.setMsg(msg));
        }
        passageTypeService.updateById(passageType);
        return ResponseResultUtil.renderSuccess("更新成功");
    }

    @DeleteMapping("/delete")
    @ApiOperation("根据id删除文章类型")
    public ResponseResult delete(String id) {
        passageTypeService.deleteById(id);
        return ResponseResultUtil.renderSuccess("删除文章类型成功");
    }

    @PostMapping
    @ApiOperation("添加文章类型")
    public ResponseResult add(@Valid @RequestBody PassageType passageType, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            String msg = bindingResult.getFieldError().getDefaultMessage();
            throw new ParamInvalidException(ErrorEnum.INVALIDATE_PARAM_EXCEPTION.setMsg(msg));
        }
        passageTypeService.insert(passageType);
        return ResponseResultUtil.renderSuccess("添加文章类型成功");
    }
}

