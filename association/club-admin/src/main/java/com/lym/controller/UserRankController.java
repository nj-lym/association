package com.lym.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lym
 * @since 2021-03-03
 */
@RestController
@RequestMapping("/userRank")
@ApiIgnore
public class UserRankController {

}

