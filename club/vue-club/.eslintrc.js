// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    browser: true,
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "quotes": [1, "single"], //引号类型 `` "" ''
    "no-mixed-spaces-and-tabs":0,
    "space-before-function-paren": [0, "always"],//函数定义时括号前面要不要有空格
    "space-before-blocks": [0, "always"],//不以新行开始的块{前面要不要有空格
    "no-tabs": "off",
    'indent': 0,
    'comma-dangle': 0,
    'no-trailing-spaces': 0,
    'eol-last': 0,
    'semi': 0,
    'eqeqeq': ['off']
  }
}
